import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import stats
import statsmodels.api as sm
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf




#Question 1

def blankNoiseGaussian():
    return np.random.normal(0,0.5,1000);

#Question 2
def printBlankNoise(serie):
    plt.plot(serie)
    plt.title("Série de bruit blanc obtenue")
    plt.show()
    print('Question 2 : La fonction a l\'air d\'être stationnaire');

#Question 3
fig = plt.figure(figsize=(12,8))
def ACF_PACFfigure(fig):
    ax1 = fig.add_subplot(211)
    fig = sm.graphics.tsa.plot_acf(blankNoiseGaussian(), lags=20, ax = ax1)
    ax2 = fig.add_subplot(212)
    fig = sm.graphics.tsa.plot_pacf(blankNoiseGaussian(), lags=20, ax= ax2)
    plt.show()
    print("C'est comme si on avait établit cette série grâce à un modèle ARMA(1,0)")

#Question 4

def ADFTest(serie):
    ADFTest = sm.tsa.stattools.adfuller(serie)
    print('ADFTest : ',ADFTest)

def KPSSTest(serie):
    KPSS = sm.tsa.stattools.kpss(serie)
    print("KPSS Test: ",KPSS)

def run() :
    #print(blankNoiseGaussian())
    #printBlankNoise(blankNoiseGaussian()) #Question 1 & 2
    ACF_PACFfigure(fig)
    ADFTest(blankNoiseGaussian())
    KPSSTest(blankNoiseGaussian())