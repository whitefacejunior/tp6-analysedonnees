import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_squared_error
import pandas as pd
from scipy import stats
import statsmodels.api as sm
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf


# Question 1
import Exercice2


def xTest(csv, column):
    xTrainList = []
    for i in range(51, 57):
        if str(csv['Date'][i]).startswith('2020'):
            xTrainList.append(csv[column][i])
    return xTrainList

def xTrain(csv, column):
    xTestList = []
    for i in range(0, len(csv['Date'])):
        if str(csv['Date'][i]).startswith('2020'): a = True
        else: xTestList.append(csv[column][i])
    return xTestList

#Question 2

def ACF_PACFfigure(fig, list):

    ax1 = fig.add_subplot(211)
    fig = sm.graphics.tsa.plot_acf(list, lags=20, ax = ax1)
    ax2 = fig.add_subplot(212)
    fig = sm.graphics.tsa.plot_pacf(list, lags=20, ax= ax2)
    plt.show()


salesCsv = pd.read_csv('sales.csv')
fig = plt.figure(figsize=(12,8))

#Question 3, 4 et 5  : ARIMA modèle
def ARIMAtest(list):
    model = ARIMA(list, order=(3,1,1))
    results = model.fit()
    return results

def ARIMAresid(ARIMAsummary):
    return ARIMAsummary.resid

#Question 6

def shapiroWilk(list):
    print('Résultat de ShapiroWilk ',stats.shapiro(list))

def boxPierce(list):
    print('Résultat de BoxPierce ', sm.stats.diagnostic.acorr_ljungbox(list, return_df=True, lags=1))

#Question 7

def ARIMAPredict(ARIMAResult, numberMonthPredict):
    predict = ARIMAResult.predict(start=51, end=51+numberMonthPredict)
    ARIMAResult.plot_predict(dynamic=False, start=51, end= 51+numberMonthPredict)
    plt.show()
    return predict

def erreurQuadratique(listA, listB):
    return mean_squared_error(listA,listB)

def run():
    #print(xTrain(salesCsv, 'Fuel'))
    #ACF_PACFfigure(fig, )
    #formatxTrain = Exercice2.formatData(xTrain(salesCsv, "Food"))
    x = ARIMAtest(xTrain(salesCsv, 'Fuel'))
    #print(x.summary())
    #print(ARIMAresid(x))
    #print(shapiroWilk(ARIMAresid(x)))
    #print(boxPierce(ARIMAresid(x)))
    #formatTest = Exercice2.formatData(salesCsv.Fuel)
    y = ARIMAtest(salesCsv.Fuel)
    predict = ARIMAPredict(y,5)
    print("Erreur quadratique : ", erreurQuadratique(predict, xTest(salesCsv, "Fuel")))





