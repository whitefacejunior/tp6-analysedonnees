import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import stats
import statsmodels.api as sm
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf


#Question 2

def xTrain(csv, column):
    xTestList = []
    for i in range(0, len(csv['Date'])):
        if str(csv['Date'][i]).startswith('2020'): a = True
        else: xTestList.append(csv[column][i])
    return xTestList

#Modifier pour la bouffe
def shapiroWilk(list):
    salesCsv = pd.read_csv('sales.csv')
    print('Résultat de ShapiroWilk ', stats.shapiro(list))

def boxPierce(list):
    salesCsv = pd.read_csv('sales.csv')
    print('Résultat de BoxPierce ', sm.stats.diagnostic.acorr_ljungbox(list, return_df=True, lags=1))

def ACF_PACFfigure(fig, list):
    ax1 = fig.add_subplot(211)
    fig = sm.graphics.tsa.plot_acf(list, lags=20, ax = ax1)
    ax2 = fig.add_subplot(212)
    fig = sm.graphics.tsa.plot_pacf(list, lags=20, ax= ax2)
    plt.show()


def ADFTest(list):
    ADFTest = sm.tsa.stattools.adfuller(list)
    print(ADFTest)

def KPSS(list):
    KPSS = sm.tsa.stattools.kpss(list)
    print("Test" , KPSS)

#PARTIE2
def formatData(list):
    differenceValue = []
    for i in range(1, len(list)):
        differenceValue.append(list[i]-list[i-1])
    return differenceValue

def printData(data):
    plt.plot(data)
    plt.title("Graphique obtenu sur la série de la différence de carburant")
    plt.show()
    print("Pour le fuel, c'est surement stationnaire")

#UTILS
fig = plt.figure(figsize=(12,8))
salesCsv = pd.read_csv('sales.csv')
def run():
    print(xTrain(salesCsv, "Food"))
    shapiroWilk(xTrain(salesCsv, "Food"))
    boxPierce(xTrain(salesCsv, "Food"))
    #ADFTest(salesCsv['Fuel'])
    #ACF_PACFfigure(fig, salesCsv['Fuel'])


def runPART2():
    list = xTrain(salesCsv, 'Fuel')
    listFormat = formatData(list)
    print(listFormat)
    ACF_PACFfigure(fig, listFormat)
    ADFTest(listFormat)
    KPSS(listFormat)
